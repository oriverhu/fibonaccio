import { Component, OnInit } from '@angular/core';
import { FibService } from '../services/fib.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styles: [
  ]
})
export class FormComponent implements OnInit {

  sucesion:any = null;
  enesimo:any = null;

  constructor( private fib: FibService) { }

  ngOnInit(): void {
  }


  ValidarNum(event: any) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }

  formulaFibonacci(num:any) {
    if(num){

      const pattern = /[0-9\+\-\ ]/;

      if (pattern.test(num)) {
           this.fib.getSucesion(num).subscribe((data:any) => {
            console.log(data)
            this.sucesion = data.response.sucesion
            this.enesimo = data.response.enesimo
            },(e)=>{
              console.log(e)
            })
      }
 

    }else{
      this.sucesion=null
    }
  }

}
