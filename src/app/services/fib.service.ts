import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FibService {

  constructor( private http:HttpClient ) { }

   getSucesion( num: any){
    return this.http.get("http://localhost:3000/sucesion/"+num).pipe( map(data => data  ))
    //  return response;
  }
}
