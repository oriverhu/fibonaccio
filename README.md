# Fibonacci

Proyecto creado con [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Instalaciones necesarias
- Node
- Nodemon
- Typescript
- Angular Cli

## Api server
Desde la consola, navegar a la carpeta del proyecto API y ejecutar: nodemon server 
--> esto habilitará el servidor en un puerto local 3000

## Development server

Desde la consola, navegar a la carpeta del proyecto y correr: `ng serve`  
Y luego desde un navegador web ingresar a: `http://localhost:4200/`. 

O desde la consola simplemente correr: `ng serve -o`