const express = require('express');
const app = express();
const port = process.env.PORT || 3000;


app.use(function(req, res, next) { res.header('Access-Control-Allow-Origin', '*'); res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method'); res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE'); res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE'); next();});

app.get('/sucesion/:id', (req, res) => {
    let num = parseInt(req.params.id);

    let response = {};
    let numeros=[0,1];
    for (let i = 2; i < num; i++) {
        console.log(i)
        numeros[i] = numeros[i - 2] + numeros[i - 1];
    }
    response.sucesion = numeros;
    response.enesimo = numeros[numeros.length-2]+numeros[numeros.length-1]

    res.json({
        ok:true,
        response
    })
})

app.listen(port, () => {
    console.log("Escuchando puerto " + port);
})